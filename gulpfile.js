import gulp from "gulp";
import htmlmin from "gulp-htmlmin";
import concat from "gulp-concat";
import terser from "gulp-terser";
import clean from "gulp-clean";
import browserSync from "browser-sync";
const BS = browserSync.create();
import dartSass from "sass";
import gulpSass from "gulp-sass";
import autoprefixer from "gulp-autoprefixer";
import cssMin from "gulp-clean-css";
import imagemin from "gulp-imagemin";
const sass = gulpSass(dartSass);

export const html = () =>
  gulp
    .src("./src/**/*.html")
    .pipe(htmlmin({ collapseWhitespace: true }))
    .pipe(gulp.dest("./dist"));

export const buildStyles = () =>
  gulp
    .src("./src/sass/**/*.scss")
    .pipe(sass().on("error", sass.logError))
    .pipe(gulp.dest("./dist/css"));

export const js = () =>
  gulp
    .src("./src/scripts/*.js")
    .pipe(concat("scripts.min.js"))
    .pipe(terser())
    .pipe(gulp.dest("./dist/scripts"));

export const cleanDist = () =>
  gulp.src("./dist", { read: false, allowEmpty: true }).pipe(clean());

export const imgMin = () =>
  gulp.src("./src/images/**/*").pipe(imagemin()).pipe(gulp.dest("dist/images"));

export const sassCompile = () =>
  gulp.src("./src/**/*.scss").pipe(sass()).pipe(gulp.dest("./dist/css"));

export const css = () =>
  gulp
    .src("./src/**/*.css")
    .pipe(
      autoprefixer({
        cascade: false,
      })
    )
    .pipe(concat("main.min.css"))
    .pipe(cssMin())
    .pipe(gulp.dest("./dist/css"));

const watcher = () => {
  browserSync.init({
    server: {
      baseDir: "./dist",
    },
  });

  gulp.watch("./src/index.html").on("change", browserSync.reload);
  gulp
    .watch("./src/sass/**/*.scss", buildStyles)
    .on("change", browserSync.reload);
  gulp.watch("./src/scripts/*.js", js).on("change", browserSync.reload);
  gulp.watch("./src/images/**/*", imgMin).on("change", browserSync.reload);
};

const cleanBuild = () => gulp.src("./dist", { allowEmpty: true }).pipe(clean());

<<<<<<< HEAD
const build = gulp.series(css, js, html);

gulp.task("build", gulp.series(cleanBuild, gulp.parallel(imgMin, build))),
  gulp.task("dev", gulp.series(build, imgMin, watcher));
=======
const build = gulp.series(buildStyles, js);

gulp.task("build", gulp.series(cleanBuild, gulp.parallel(imgMin, build))),
gulp.task("dev", gulp.series(build, imgMin, watcher));
>>>>>>> 5e6aa679e7961aad87d6cec043d512c60224376a
